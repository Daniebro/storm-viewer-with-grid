//
//  PictureCell.swift
//  Project1
//
//  Created by Danni Brito on 12/17/19.
//  Copyright © 2019 Danni André. All rights reserved.
//

import UIKit

class PictureCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var name: UILabel!
}
