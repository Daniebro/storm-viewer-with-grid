//
//  Picture.swift
//  Project1
//
//  Created by Danni Brito on 12/17/19.
//  Copyright © 2019 Danni André. All rights reserved.
//

import UIKit

class Picture: NSObject {
    var name: String
    var image: String
    
    init(name: String, image: String) {
        self.name = name
        self.image = image
    }
}
